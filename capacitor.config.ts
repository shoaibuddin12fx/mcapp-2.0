import { CapacitorConfig } from "@capacitor/cli";

const config: CapacitorConfig = {
  appId: "io.ionic.ionicnecco",
  appName: "Material Control - Loginets",
  webDir: "www",
  bundledWebRuntime: false,
  cordova: {
    preferences: {
      "android-minSdkVersion": "27",
      "android-targetSdkVersion": "30",
      ScrollEnabled: "false",
      BackupWebStorage: "none",
      SplashMaintainAspectRatio: "true",
      FadeSplashScreenDuration: "0",
      SplashShowOnlyFirstTime: "false",
      SplashScreen: "none",
      SplashScreenDelay: "0",
      ShowSplashScreen: "false",
      AutoHideSplashScreen: "true",
    },
  },
  plugins: {
    SplashScreen: {
      launchShowDuration: 0,
      launchAutoHide: true,
      backgroundColor: "#ffffffff",
      androidSplashResourceName: "splash",
      androidScaleType: "CENTER_CROP",
      showSpinner: false,
      splashFullScreen: true,
      splashImmersive: true,
      layoutName: "launch_screen",
      useDialog: true,
    },
  },
};

export default config;
