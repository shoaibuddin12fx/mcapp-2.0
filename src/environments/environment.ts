// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  //SERVER_URL:'https://valpas.testi.loginets.com',
  //SERVER_URL:'https://valpas.testi.loginets.com',
  // SERVER_URL:'https://mc.testi.loginets.com',
  // SERVER_URL: "https://upm.loginets.com/mc",
  SERVER_URL_ONE: "https://upm.loginets.com/mc/",
  SERVER_URL_TWO: "https://mc.testi.loginets.com",
  // SERVER_URL: "https://mc.testi.loginets.com",
  HAS_LOGGED_IN: "hasLoggedIn",
  USER_NAME: "USER_NAME",
  TOKEN: "TOKEN",
  REFRESH_TOKEN: "REFRESH_TOKEN",
  ACCOUNT_ID: "ACCOUNT_ID",
  PROJECT_CODE: "PROJECT_CODE_MC",
  STATUS_CODE: "STATUS_CODE_MC",
  PACKAGE_STORAGE: "PACKAGE_STORAGE",
  PACKAGE_COMMENT: "PACKAGE_COMMENT",
  SHIPMENT_STORAGE: "SHIPMENT_STORAGE",
  SHIPMENT_COMMENT: "SHIPMENT_COMMENT",
  CONTAINER_STORAGE: "CONTAINER_STORAGE",
  CONTAINER_COMMENT: "CONTAINER_COMMENT",
  IS_SUPERVISOR: "false",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
