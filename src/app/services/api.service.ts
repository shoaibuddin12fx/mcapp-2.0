import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  // url = environment.SERVER_URL;

  constructor(public http: HttpClient) {}

  get(endpoint: string, params?: any, reqOpts?: any) {
    let url = localStorage.getItem("app_Url");
    return this.http.get(url + "/" + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    let url = localStorage.getItem("app_Url");
    return this.http.post(url + "/" + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    let url = localStorage.getItem("app_Url");
    return this.http.put(url + "/" + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    let url = localStorage.getItem("app_Url");
    return this.http.delete(url + "/" + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    let url = localStorage.getItem("app_Url");
    return this.http.patch(url + "/" + endpoint, body, reqOpts);
  }
}
