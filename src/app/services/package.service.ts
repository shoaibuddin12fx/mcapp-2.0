import { IService } from "./IService";
// import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AppSettings } from "./app-settings";
import { LoadingService } from "./loading-service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { TranslationService } from "./translation-service.service";
import { UtilityService } from "./utility.service";
import { Router } from "@angular/router";
import { SqliteService } from "./sqlite.service";
import { EventsService } from "./events.service";
import { BobjectsService } from "./bobjects.service";

@Injectable({
  providedIn: "root",
})
export class PackageService implements IService {
  confirmationText;
  constructor(
    private loadingService: LoadingService,
    public translation: TranslationService,
    public utility: UtilityService,
    public sqlite: SqliteService,
    public router: Router,
    public events: EventsService,
    private http: HttpClient,
    public bObject: BobjectsService
  ) {}

  getAllThemes(): any[] {
    throw new Error("Method not implemented.");
  }
  getTitle(): string {
    throw new Error("Method not implemented.");
  }
  load(menuItem: any) {
    throw new Error("Method not implemented.");
  }

  getPackageList(search = "", offset = 0): Promise<any> {
    return new Promise(async (resolve) => {
      let count = await this.sqlite.getPackageCount();
      // logic to load data after 24 hours
      var time = localStorage.getItem("package_call");
      var callagain = false;
      if (!time) {
        var ts = Math.round(new Date().getTime()).toString();
        localStorage.setItem("package_call", ts);
        callagain = true;
      }

      var timeOne = parseInt(localStorage.getItem("package_call"));
      var timeTwo = new Date().getTime();
      var diff = timeTwo - timeOne;
      if (diff / 86400000 > 1) {
        callagain = true;
      }
      if (count > 0 && !callagain) {
        resolve(this.getListBySqlite(search, offset));
      } else {
        this.confirmationText = await this.translation.getTranslateKey(
          "Package_Loading_Confirmation"
        );
        this.utility
          .presentConfirm("Yes", "No", this.confirmationText)
          .then((val) => {
            if (val == true) {
              this.sqlite.deleteAllPackages();
              resolve(this.getListByUrl(search, offset));
            } else {
              resolve(this.getListBySqlite(search, offset));
            }
          });
      }
    });
  }

  getListBySqlite(search, offset) {
    return new Promise(async (resolve) => {
      let obj = await this.sqlite.getPackageInDatabase(search, offset);
      console.log(obj);
      resolve(obj);
    });
  }

  insertDataInSqlite(data) {
    return new Promise(async (resolve) => {
      await this.sqlite.setPackageInDatabase(data);
      resolve(true);
    });
  }

  getListByUrl(search = "", offset = 0) {
    return new Promise((resolve) => {
      var url = localStorage.getItem("app_Url") + "/rest/packages/list";

      var headers = new HttpHeaders({
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
      });

      let projectId = localStorage.getItem(environment.PROJECT_CODE);

      if (!projectId) {
        this.translation
          .getTranslateKey("project_selection_error")
          .then((v) => {
            this.utility.presentFailureToast(v);
          });

        this.router.navigateByUrl("setting");
        return;
      }

      let request = {
        username: localStorage.getItem(environment.USER_NAME),
        token: localStorage.getItem(environment.TOKEN),
        locale: "en_US",
        projectId: localStorage.getItem(environment.PROJECT_CODE),
        statusCodes: localStorage.getItem(environment.STATUS_CODE).split(","),
      };
      this.http.post(url, request, { headers: headers }).subscribe(
        (d: any) => {
          console.log(d);

          let data = d.responseData.entities;
          this.insertDataInSqlite(data).then(() => {
            this.loadingService.hideLoader();
            resolve(this.getListBySqlite(search, offset));
          });
        },
        (error) => {
          let obj = {
            offset: -1,
            data: [],
          };
          resolve(obj);
        }
      );
    });
  }

  getOrderLineDetailByPackageId(id): Promise<any[]> {
    return new Promise(async (resolve) => {
      var url = localStorage.getItem("app_Url") + "/rest/packages";

      var headers = new HttpHeaders({
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
      });

      let projectId = localStorage.getItem(environment.PROJECT_CODE);

      let request = {
        username: localStorage.getItem(environment.USER_NAME),
        token: localStorage.getItem(environment.TOKEN),
        id: id,
        locale: "en_US",
      };
      let bobj = await this.bObject.prepareObject(null);

      const merged = { ...bobj, ...request };
      this.http.post(url, merged, { headers: headers }).subscribe((d: any) => {
        console.log(d);

        let data = d.responseData.packages[0].orderlines;
        resolve(data);
      });
    });
  }
}
