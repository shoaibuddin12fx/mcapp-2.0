import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { SqliteService } from "./sqlite.service";
import { UtilityService } from "./utility.service";
import { Observable, from } from "rxjs";
import { switchMap } from "rxjs/operators";
import { StorageService } from "./storage.service";
import { EncryptService } from "../encrypt.service";
import { BobjectsService } from "./bobjects.service";
import { GeolocationsService } from "./geolocations.service";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class InterceptorResponseService implements HttpInterceptor {
  constructor(
    private storage: StorageService,
    public utility: UtilityService,
    public encrypt: EncryptService,
    public bObject: BobjectsService,
    public geo: GeolocationsService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return from(this.getCoords()).pipe(
      switchMap((data) => {
        const cloneRequest = this.addParams(req, data);
        return next.handle(cloneRequest);
      })
    );
  }

  getCoords() {
    return new Promise(async (resolve) => {
      // sk for geolocation permission
      // if (environment.SERVER_URL.includes("testi")) {
      //   const coords = await this.geo.getLocationCoordinates();
      //   let obj = await this.bObject.prepareObject(coords);
      //   console.log(obj);
      //   resolve(obj);
      // } else {
      let obj = await this.bObject.prepareObject(null);
      console.log(obj);
      resolve(obj);
      // }
    });
  }

  private addParams(request: HttpRequest<any>, data = {}) {
    var bobj = request.body;
    if (bobj) {
      bobj = { ...bobj, ...data };
      console.log(bobj);
    }

    let clone = request.clone({ body: bobj });
    return clone;
  }
}
