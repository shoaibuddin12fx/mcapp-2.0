import { IService } from "./IService";
// import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AppSettings } from "./app-settings";
import { LoadingService } from "./loading-service";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { EncryptService } from "../encrypt.service";
import { BobjectsService } from "./bobjects.service";
import { UserService } from "./user.service";

@Injectable({ providedIn: "root" })
export class LoginService implements IService {
  loginDetails;
  constructor(
    public http: HttpClient,
    public encrypt: EncryptService,
    public user: UserService
  ) {
    this.getLoginDetails();
  }

  getTitle = (): string => "Login pages";

  getAllThemes = (): Array<any> => {
    return [
      { url: "login/0", title: "Login + logo 1", theme: "layout1" },
      { url: "login/1", title: "Login + logo 2", theme: "layout2" },
    ];
  };

  getDataForTheme = (menuItem: any): Array<any> => {
    return this[
      "getDataFor" +
        menuItem.theme.charAt(0).toUpperCase() +
        menuItem.theme.slice(1)
    ]();
  };

  //* Data Set for page 1
  getDataForLayout1 = (): any => {
    return {
      headerTitle: "Login + logo 1",
      username: "Enter your username",
      password: "Enter your password",
      labelUsername: "USERNAME",
      labelPassword: "PASSWORD",
      register: "Register now!",
      forgotPassword: "Forgot password?",
      login: "Login",
      loginFacebook: "Login With Facebook",
      subtitle: "Login",
      title: "Login to your account",
      description: "Don't have account?",
      skip: "Skip",
      logo: "assets/imgs/logo/logo.png",
    };
  };

  //* Data Set for page 2
  getDataForLayout2 = (): any => {
    return {
      headerTitle: "Login + logo 2",
      forgotPassword: "Forgot password?",
      subtitle: "Welcome",
      labelUsername: "USERNAME",
      labelPassword: "PASSWORD",
      title: "Login to your account",
      username: "Enter your username",
      password: "Enter your password",
      register: "Register",
      login: "Login",
      skip: "Skip",
      facebook: "Facebook",
      twitter: "Twitter",
      google: "Google",
      pinterest: "Pinterest",
      description: "Don't have account?",
      logo: "assets/imgs/logo/2.png",
    };
  };

  load(item: any): Observable<any> {
    //this.loadingService.show();
    return new Observable((observer) => {
      //this.loadingService.hide();
      observer.next(this.getDataForTheme(item));
      observer.complete();
    });
  }
  async getLoginDetails() {
    this.loginDetails = await this.user.getloginDetails();
  }
  login(param: any): Observable<any> {
    var url = localStorage.getItem("app_Url") + "/rest/login";
    var dataObj = {
      username: this.encrypt.set(param.username),
      password: this.encrypt.set(param.password),
      loginDetails: this.loginDetails,
    };

    console.log(dataObj);

    return this.http.post(url, dataObj, { observe: "response" });

    /* return new Observable(observer => {
        this.af
          .object('login/' + item.theme)
          .valueChanges()
          .subscribe(snapshot => {
            this.loadingService.hide();
            observer.next(snapshot);
            observer.complete();
          }, err => {
            this.loadingService.hide();
            observer.error([]);
            observer.complete();
          });
      }); */
  }
}
