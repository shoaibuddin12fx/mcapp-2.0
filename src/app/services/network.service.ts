import { Injectable } from "@angular/core";
import { Platform } from "@ionic/angular";
import { ApiService } from "./api.service";
import { EventsService } from "./events.service";
import { SqliteService } from "./sqlite.service";
import { UtilityService } from "./utility.service";

@Injectable({
  providedIn: "root",
})
export class NetworkService {
  sent;
  login(data) {
    return this.httpPostResponse("login", data, null, true);
  }

  getUser(data, loader = false, error = false) {
    return this.httpPostResponse("users", data, null, loader, error);
  }

  getItsQrcode(data) {
    return this.httpPostResponse("tasks/qrcode", data, null, true);
  }

  getItsQrcodeInOut(data) {
    return this.httpPostResponse("tasks/inOut", data, null, true);
  }

  getAllProjects(data) {
    return this.httpPostResponse("projects/list", data, null, true);
  }

  getAllLitteras(data) {
    return this.httpPostResponse("litteras", data, null, true);
  }

  getAllWorks(data) {
    return this.httpPostResponse("orders/list", data, null, true);
  }
  getSuppliers(data) {
    return this.httpPostResponse("orders/suppliers", data, null, false);
  }

  getStockItemDetail(data) {
    return this.httpPostResponse("/rest/stockitems", data, null, false);
  }

  getOrderItemDetail(data) {
    return this.httpPostResponse("/rest/orders/lines", data, null, false);
  }

  getTokenRefresh(data) {
    return this.httpPostResponse("login/refresh", data, null, false);
  }

  getPackageDetailByContainerId(data) {
    return this.httpPostResponse("containers", data, null, false);
  }

  getOrderLineDetailByPackageId(data) {
    return this.httpPostResponse("packages", data, null, false);
  }

  constructor(
    public utility: UtilityService,
    public api: ApiService,
    private events: EventsService,
    private platform: Platform,
    public sqlite: SqliteService
  ) {
    // console.log('Hello NetworkProvider Provider');
  }

  httpPostResponse(
    key,
    data,
    id = null,
    showloader = false,
    showError = true,
    contenttype = "application/json"
  ) {
    return this.httpResponse(
      "post",
      key,
      data,
      id,
      showloader,
      showError,
      contenttype
    );
  }

  httpGetResponse(
    key,
    id = null,
    showloader = false,
    showError = true,
    contenttype = "application/json"
  ) {
    return this.httpResponse(
      "get",
      key,
      {},
      id,
      showloader,
      showError,
      contenttype
    );
  }

  // default 'Content-Type': 'application/json',
  httpResponse(
    type = "get",
    key,
    data,
    id = null,
    showloader = false,
    showError = true,
    contenttype = "application/json"
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      console.log("running http response function");
      this.sent = data ? data : {};
      console.log(key);
      if (showloader == true) {
        this.utility.showLoader();
      }

      const _id = id ? "/" + id : "";
      const url = key + _id;
      console.info(url);

      let values = {
        type: type,
        url: url,
        sent: JSON.stringify(this.sent),
        data: JSON.stringify(data),
        timestamp: Date.now() / 1000,
      };
      console.log("here are log values", values);
      this.addLogsInDevice({
        type: type,
        url: url,
        sent: JSON.stringify(this.sent),
        data: JSON.stringify(data),
        timestamp: Date.now() / 1000,
      });
      const seq =
        type == "get" ? this.api.get(url, {}) : this.api.post(url, data);

      seq.subscribe(
        (data: any) => {
          console.log(data);
          let res = data["responseData"];
          if (showloader == true) {
            this.utility.hideLoader();
          }

          // is jaga logs girne chaheye
          console.log(data["code"]);
          if (data["code"] != 200) {
            if (showError) {
              const myVar = data["responseData"];
              if (typeof myVar === "string" || myVar instanceof String) {
                this.utility.presentSuccessToast(data["responseData"]);
              } else {
                if (!data["responseData"]) {
                  this.utility.presentSuccessToast("Request Failed");
                  reject(null);
                  return;
                }
                if (!data["responseData"]["feedback"]) {
                  this.utility.presentSuccessToast("Request Failed");
                  reject(null);
                  return;
                }

                let failures = data["responseData"]["feedback"]["failures"];
                if (failures.length > 0) {
                  let msg = failures[0]["name"];
                  if (msg) {
                    this.utility.presentSuccessToast(msg);
                  } else {
                    this.utility.presentSuccessToast("Request Failed");
                  }
                } else {
                  this.utility.presentSuccessToast("Request Failed");
                }
              }
            }

            // ya to yaha honaa he
            reject(null);
          } else {
            // ya phr yahan
            console.log("hello response", { data });
            console.log("http response", this.sent);
            resolve(res);
          }
        },
        (err) => {
          this.addLogsInDevice({
            type: type,
            url: url,
            sent: JSON.stringify(this.sent),
            data: JSON.stringify(err),
            timestamp: Date.now() / 1000,
          });
          let error = err;
          if (showloader == true) {
            this.utility.hideLoader();
          }

          if (showError) {
            this.utility.presentFailureToast(error["responseData"]);
          }

          console.log(err);

          reject(null);
        }
      );
    });
  }
  async addLogsInDevice(data) {
    console.log("we got data", data);
    if (data.insert == false) {
      this.sqlite.setLogInDatabase(data);
    }
  }
}
