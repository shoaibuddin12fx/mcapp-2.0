import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { BobjectsService } from "./bobjects.service";

@Injectable({
  providedIn: "root",
})
export class BookstoreService {
  bobj;

  constructor(private http: HttpClient) {}

  insertShipment(comment, arr: []): Observable<any> {
    var url = localStorage.getItem("app_Url") + "/rest/shipments/confirm";

    var headers = new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    });

    let request = {
      username: localStorage.getItem(environment.USER_NAME),
      token: localStorage.getItem(environment.TOKEN),
      locale: "fi",
      ids: arr,
      confirmDate: new Date().getTime(),
      location: `${comment.value.area}`,
      comment: comment.value.name,
      latitude: comment.value.latitude,
      longitude: comment.value.longitude,
    };
    return this.http.post(url, request, { headers: headers });
  }

  insertContainer(comment, arr: []): Observable<any> {
    var url = localStorage.getItem("app_Url") + "/rest/containers/confirm";

    var headers = new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    });

    console.log(comment);
    let request = {
      username: localStorage.getItem(environment.USER_NAME),
      token: localStorage.getItem(environment.TOKEN),
      locale: "fi",
      ids: arr,
      confirmDate: new Date().getTime(),
      location: `${comment.value.area}`,
      comment: comment.value.name,
      latitude: comment.value.latitude,
      longitude: comment.value.longitude,
    };
    console.log("getting request", request);
    return this.http.post(url, request, { headers: headers });
  }

  insertPackages(comment, arr: []): Observable<any> {
    var url = localStorage.getItem("app_Url") + "/rest/packages/confirm";

    var headers = new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    });

    let request = {
      username: localStorage.getItem(environment.USER_NAME),
      token: localStorage.getItem(environment.TOKEN),
      locale: "fi",
      ids: arr,
      confirmDate: new Date().getTime(),
      location: `${comment.value.area}`,
      comment: comment.value.name,
      latitude: comment.value.latitude,
      longitude: comment.value.longitude,
    };
    return this.http.post(url, request, { headers: headers });
  }

  insertStockitem(comment, arr: []): Observable<any> {
    var url = localStorage.getItem("app_Url") + "/rest/stockitems/confirm";

    var headers = new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    });

    let request = {
      username: localStorage.getItem(environment.USER_NAME),
      token: localStorage.getItem(environment.TOKEN),
      locale: "fi",
      ids: arr,
      confirmDate: new Date().getTime(),
      location: `${comment.value.area}`,
      comment: comment.value.name,
      latitude: comment.value.latitude,
      longitude: comment.value.longitude,
    };
    return this.http.post(url, request, { headers: headers });
  }
}
