import { Injectable } from '@angular/core';
import { Observable, observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  constructor(private http:HttpClient) { }

  saveSetting(formdata) : Observable<any>{
    console.log(formdata);
    localStorage.setItem(environment.PROJECT_CODE,formdata.value.project);
    localStorage.setItem(environment.STATUS_CODE,formdata.value.statuscode);
    return of('setting saved');
  }
}
