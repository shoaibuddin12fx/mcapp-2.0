import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AppSettings } from "./app-settings";
import { LoadingService } from "./loading-service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class ProjectService {
  constructor(
    private loadingService: LoadingService,
    private http: HttpClient
  ) {}

  getProjectTags(): Observable<any> {
    var url = localStorage.getItem("app_Url") + "/rest/projects/tags";

    var headers = new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    });

    let request = {
      username: localStorage.getItem(environment.USER_NAME),
      token: localStorage.getItem(environment.TOKEN),
      accountId: localStorage.getItem(environment.ACCOUNT_ID),
      locale: "en_US",
    };
    return this.http.post(url, request, { headers: headers });
  }
}
