import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';


@Injectable({
  providedIn: 'root'
})
export class CommonService {

  shipmentData = new BehaviorSubject([]);
  packageData = new BehaviorSubject([]);
  containerData = new BehaviorSubject([]);
  
  fromQRCode = new BehaviorSubject([]);
  constructor() { 
  }

  addShipmentData(reserve: []) {
    this.shipmentData.next(reserve)
  }

  addPackageData(reserve: any) {
    this.packageData.next(reserve)
  }

  addContainerData(reserve: any) {
    this.containerData.next(reserve)
  }

  addFromQRCode(val: any){
    this.fromQRCode.next(val);
  }
}
