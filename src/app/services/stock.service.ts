import { Injectable, Injector } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { SqliteService } from "./sqlite.service";
import { BasePage } from "../pages/base-page/base-page";
import { BobjectsService } from "./bobjects.service";
import { LoadingService } from "./loading-service";

@Injectable({
  providedIn: "root",
})
export class StockService extends BasePage {
  constructor(
    private http: HttpClient,
    public sqlite: SqliteService,
    injector: Injector,
    public bObject: BobjectsService,
    public loadingService: LoadingService
  ) {
    super(injector);
  }

  searchMaterial(filters: any): Observable<any> {
    var url = localStorage.getItem("app_Url") + "/rest/search/material";

    var headers = new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    });

    let request = {
      username: localStorage.getItem(environment.USER_NAME),
      token: localStorage.getItem(environment.TOKEN),
      searchParameters: {
        ...filters,
      },
      isMobile: true,
    };
    return this.http.post(url, request, { headers: headers });
  }

  getStockList(search = "", offset = 0, mode): Promise<any> {
    return new Promise(async (resolve) => {
      resolve(this.getStockOrMaterialListBySqlite(search, offset, mode));
    });
  }

  searchStock(filters: any): Observable<any> {
    var url = localStorage.getItem("app_Url") + "/rest/stockitems/list";

    var headers = new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    });
    console.log("Getting Stock Filters", filters);
    let stockRequestObj = {
      texts: Number(filters.workNo) ? [Number(filters.workNo)] : [],
      posNo: filters.posNo ? filters.posNo : "",
      description: filters.description ? filters.description : "",
      productCode: filters.partNo ? filters.partNo : "",
      poNo: filters.poNo ? filters.poNo : "",
      drawingNo: filters.drawingNo ? filters.drawingNo : "",
      ownerId: filters.projectId,
    };
    console.log("filters for stock applied!", { stockRequestObj });
    let request = {
      username: localStorage.getItem(environment.USER_NAME),
      token: localStorage.getItem(environment.TOKEN),
      ...stockRequestObj,
      isMobile: true,
    };
    return this.http.post(url, request, { headers: headers });
  }

  getStockOrMaterialListBySqlite(search, offset, mode) {
    return new Promise(async (resolve) => {
      if (mode == "Stock Items") {
        let obj = await this.sqlite.getStockInDatabase(search, offset);
        console.log("get list from sqlite: ", { obj });
        resolve(obj);
      } else {
        let obj = await this.sqlite.getMaterialInDatabase(search, offset);
        console.log("get list from sqlite: ", { obj });
        resolve(obj);
      }
    });
  }

  insertStockOrMaterialDataInSqlite(data, mode) {
    return new Promise(async (resolve) => {
      if (mode == "Stock Items") {
        console.log("debug stock", data);
        let notNullIdData = data.filter((data) => data.id != null);
        // console.log("not null", {  });
        await this.sqlite.setStockInDatabase(notNullIdData);
        resolve(true);
      } else {
        console.log("debug stock", data);
        await this.sqlite.setMaterialInDatabase(data);
        resolve(true);
      }
    });
  }

  getStockListByUrl(filters, mode) {
    return new Promise((resolve) => {
      if (mode == "Stock Items") {
        this.searchStock(filters).subscribe(
          (v) => {
            let data = v.responseData.stockitems;
            this.insertStockOrMaterialDataInSqlite(data, mode).then(() => {
              resolve(this.getStockOrMaterialListBySqlite(null, 0, mode));
            });
          },
          (error) => {
            let obj = {
              offset: -1,
              data: [],
            };
            resolve(obj);
          }
        );
      } else {
        this.searchMaterial(filters).subscribe(
          (v) => {
            let data = v.responseData.materials;
            this.insertStockOrMaterialDataInSqlite(data, mode).then(() => {
              resolve(this.getStockOrMaterialListBySqlite(null, 0, mode));
            });
          },
          (error) => {
            let obj = {
              offset: -1,
              data: [],
            };
            resolve(obj);
          }
        );
      }
    });
  }

  async getStockItemDetails(data): Promise<any> {
    return new Promise(async (resolve) => {
      let bObj = await this.bObject.prepareObject(null);
      let stockItemDetailPayload = {
        username: localStorage.getItem(environment.USER_NAME),
        token: localStorage.getItem(environment.TOKEN),
        ownerId: localStorage.getItem(environment.PROJECT_CODE),
        id: data.id,
        ids: [],
        texts: [],
      };
      const merged = { ...bObj, ...stockItemDetailPayload };
      this.network.getStockItemDetail(merged).then((res) => {
        console.log("this is the detail for this stock item", res);
        resolve(res.entities[0]);
      });
    });
  }

  async getOrderItemDetails(data): Promise<any> {
    return new Promise(async (resolve) => {
      let bObj = await this.bObject.prepareObject(null);
      let locale = await this.translation.getLocale();
      console.log("getting materials", { data });
      let orderItemDetailPayload = {
        username: localStorage.getItem(environment.USER_NAME),
        token: localStorage.getItem(environment.TOKEN),
        ownerId: localStorage.getItem(environment.PROJECT_CODE),
        id: data.orderLineId,
        ids: null,
        locale: locale,
      };
      const merged = { ...bObj, ...orderItemDetailPayload };
      this.network.getOrderItemDetail(merged).then((res) => {
        console.log("this is the detail for this stock item", res);
        resolve(res.entities[0]);
      });
    });
  }
}
