import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { environment } from "src/environments/environment";
import { BobjectsService } from "../services/bobjects.service";
import { BookstoreService } from "../services/bookstore.service";
import { CommonService } from "../services/common.service";
import { GeolocationsService } from "../services/geolocations.service";
import { StoragelocationService } from "../services/storagelocation.service";
import { TranslationService } from "../services/translation-service.service";
import { UtilityService } from "../services/utility.service";

@Component({
  selector: "app-book-product",
  templateUrl: "./book-product.page.html",
  styleUrls: ["./book-product.page.scss"],
})
export class BookProductPage implements OnInit {
  user: FormGroup;
  tempArray: any = [];
  storageArea: any;
  fromQRCode: any;
  isfromQRCode: boolean = false;
  isSupervisor: boolean = false;

  qrcode_code;
  qrcode_description;
  qrcode_orderLine;
  qrcode_orderLineStockItems = [];
  coords;
  constructor(
    public formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public bObject: BobjectsService,
    private commonService: CommonService,
    private bookstoreService: BookstoreService,
    private storagelocationService: StoragelocationService,
    private alertCtrl: AlertController,
    public utility: UtilityService,
    private router: Router,
    public geo: GeolocationsService,
    public translation: TranslationService
  ) {
    this.isSupervisor =
      localStorage.getItem(environment.IS_SUPERVISOR) == "true";

    console.log(this.qrcode_orderLine);
    this.getQrCodeOrderLine();
    this.user = this.formBuilder.group({
      area: ["", Validators.compose([Validators.required])],
      name: [
        "",
        Validators.compose([
          Validators.minLength(2),
          Validators.maxLength(1000),
          Validators.pattern("^[A-Z a-z 0-9]*$"),
        ]),
      ],
      latitude: [""],
      longitude: [""],
    });

    this.commonService.packageData.subscribe((result) => {
      this.tempArray = result;
      console.log(this.tempArray);
    });

    this.commonService.fromQRCode.subscribe((result) => {
      this.fromQRCode = result;
      if (null != this.fromQRCode && this.fromQRCode.length > 0) {
        this.isfromQRCode = true;
      }
    });

    this.storagelocationService.getStorageAreas().subscribe((data) => {
      this.storageArea = data.responseData.entities;
    });

    this.qrcode_code = localStorage.getItem("qrcode_code")
      ? JSON.parse(localStorage.getItem("qrcode_code"))
      : "";
    localStorage.removeItem("qrcode_code");
    this.qrcode_description = localStorage.getItem("qrcode_description");
    localStorage.removeItem("qrcode_description");

    console.log(this.qrcode_code, this.qrcode_description);
  }

  ngOnInit() {
    setTimeout(async () => {
      if (undefined != localStorage.getItem(environment.PACKAGE_STORAGE)) {
        this.user.controls["area"].setValue(
          parseInt(localStorage.getItem(environment.PACKAGE_STORAGE))
        );
        this.user.controls["name"].setValue(
          localStorage.getItem(environment.PACKAGE_COMMENT)
        );
        let geoPermission = await this.geo.checkLocations();
        if (geoPermission) {
          this.coords = await this.geo.getCurrentLocationCoordinates();
          console.log("getting coordinates,", this.coords);
          this.user.controls["latitude"].setValue(this.coords.latitude);
          this.user.controls["longitude"].setValue(this.coords.longitude);
        }
      }
    }, 400);
  }

  getQrCodeOrderLine() {
    this.qrcode_orderLine = JSON.parse(
      localStorage.getItem("qrcode_orderLine")
    );
    this.qrcode_orderLineStockItems =
      this.qrcode_orderLine.orderLine.stockItems;
  }
  async onSubmit(formData) {
    localStorage.setItem(environment.PACKAGE_STORAGE, formData.value.area);
    localStorage.setItem(environment.PACKAGE_COMMENT, formData.value.name);
    this.tempArray = [localStorage.getItem("qrcode_id")];
    let geoPermission = await this.geo.checkLocations();
    if (geoPermission) {
      this.coords = await this.geo.getCurrentLocationCoordinates();
      console.log("getting coordinates,", this.coords);
      if (this.coords) {
        this.user.controls["latitude"].setValue(this.coords.latitude);
        this.user.controls["longitude"].setValue(this.coords.longitude);
        this.bookstoreService
          .insertStockitem(formData, this.tempArray)
          .subscribe(
            async (res) => {
              if (null != this.fromQRCode && this.fromQRCode.length > 0) {
                let alert = await this.alertCtrl.create({
                  message: "Booking done",
                  buttons: [
                    {
                      text: "DONE",
                      handler: (data) => {
                        this.router.navigateByUrl("home");
                      },
                    },
                    {
                      text: "SCAN MORE",
                      handler: (data) => {
                        this.router.navigateByUrl("readqrcode");
                      },
                    },
                  ],
                });
                alert.present();
              } else {
                let alert = await this.alertCtrl.create({
                  message: "Booking done",
                  buttons: [
                    {
                      text: "HOME",
                      handler: (data) => {
                        this.router.navigateByUrl("package-list");
                      },
                    },
                  ],
                });
                alert.present();
              }
            },
            async (error) => {
              let alert = await this.alertCtrl.create({
                message: "Error Occured",
                buttons: [
                  {
                    text: "OK",
                    handler: (data) => {
                      if (this.fromQRCode) {
                        this.router.navigateByUrl("home");
                      } else {
                        this.router.navigateByUrl("package-list");
                      }
                    },
                  },
                ],
              });
              alert.present();
            }
          );
      } else {
        let msg = await this.translation.getTranslateKey(
          "please_turn_on_location_permission"
        );
        this.utility.showAlert(msg);
      }
    } else {
      let msg = await this.translation.getTranslateKey(
        "please_turn_on_location_permission"
      );
      this.utility.showAlert(msg);
    }
  }

  submitDisabled() {
    if (this.isfromQRCode) {
      return this.user.invalid;
    } else {
      if (!this.isSupervisor) {
        return true;
      } else {
        return this.user.invalid;
      }
    }
  }

  cancelProcess() {
    this.router.navigateByUrl("home");
  }
}
