import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookProductPage } from './book-product.page';

describe('BookProductPage', () => {
  let component: BookProductPage;
  let fixture: ComponentFixture<BookProductPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookProductPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookProductPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
