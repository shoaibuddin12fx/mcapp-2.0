import { Component, Injector, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SettingService } from "../services/setting.service";
import { AlertController } from "@ionic/angular";
import { Router } from "@angular/router";
import { ProjectService } from "../services/project.service";
import { StatuscodeService } from "../services/statuscode.service";
import { environment } from "src/environments/environment";
import { TranslationService } from "../services/translation-service.service";
import { BasePage } from "../pages/base-page/base-page";

@Component({
  selector: "app-setting",
  templateUrl: "./setting.page.html",
  styleUrls: ["./setting.page.scss"],
  providers: [SettingService, ProjectService, StatuscodeService],
})
export class SettingPage extends BasePage implements OnInit {
  user: FormGroup;
  projectTags: any;
  stausCodes: any;
  arr: any = [];
  languages: any[] = [];
  formHidden = false;
  loading;
  constructor(
    injector: Injector,
    public formBuilder: FormBuilder,
    public settingService: SettingService,
    public projectService: ProjectService,
    public statuscodeService: StatuscodeService,
    private alertCtrl: AlertController,
    public translation: TranslationService
  ) {
    super(injector);
    this.languages = this.translation.getTranslationsList();

    this.user = this.formBuilder.group({
      project: ["", Validators.compose([Validators.required])],
      statuscode: [[9, 12], Validators.compose([Validators.required])],
    });
    this.initialize();
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.initialize();
  }

  async initialize() {
    this.loading = true;
    this.formHidden = true;
    await this.getProjects();
    await this.getStatusCodes();
    this.formHidden = false;
    let projectCode = localStorage.getItem(environment.PROJECT_CODE)
      ? localStorage.getItem(environment.PROJECT_CODE)
      : [];
    let statusCode = localStorage.getItem(environment.STATUS_CODE)
      ? localStorage.getItem(environment.STATUS_CODE)
      : "";
    setTimeout(() => {
      this.user.controls["project"].patchValue(projectCode);
      let stausCode = statusCode;
      let tmp = stausCode.split(",");
      tmp.forEach((element) => {
        this.arr.push(element);
      });

      this.user.controls["statuscode"].patchValue(this.arr);
      this.loading = false;
      console.log(this.loading);
    }, 600);
  }

  async getProjects() {
    return new Promise((resolve) => {
      this.projectService.getProjectTags().subscribe((data) => {
        this.projectTags = data.responseData.entities;
        resolve(true);
      });
    });
  }

  async getStatusCodes() {
    return new Promise((resolve) => {
      this.statuscodeService.getStatusCode().subscribe((data) => {
        this.stausCodes = data.responseData.entities;
        resolve(true);
      });
    });
  }

  async onSubmit(formData) {
    localStorage.setItem(
      environment.PROJECT_CODE,
      this.user.controls["project"].value
    );
    let stausCode = this.user.controls["statuscode"].value;
    let tmp = stausCode.join();
    localStorage.setItem(environment.STATUS_CODE, tmp);

    this.settingService.saveSetting(formData).subscribe(
      async (res) => {
        console.log(res);
        let alert = await this.alertCtrl.create({
          message: "Setting saved",
          buttons: [
            {
              text: "OK",
              handler: (data) => {
                this.events.publish("Settings: settings updated");
                this.router.navigateByUrl("home");
              },
            },
          ],
        });
        alert.present();
      },
      async (error) => {
        let alert = await this.alertCtrl.create({
          message: "Error Occured",
          buttons: [
            {
              text: "OK",
              handler: (data) => {
                this.router.navigateByUrl("dome");
              },
            },
          ],
        });
        alert.present();
      }
    );
  }

  onChangeLang(event: CustomEvent) {
    if (!event) return;

    const lang = event.detail.value;

    if (lang === "ar") {
      document.dir = "rtl";
    } else {
      document.dir = "ltr";
    }

    this.translation.useTranslation(lang);
  }

  async showLogs() {
    this.nav.push("logs");
  }
}
