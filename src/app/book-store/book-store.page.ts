import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "../services/common.service";
import { BookstoreService } from "../services/bookstore.service";
import { error } from "@angular/compiler/src/util";
import { AlertController } from "@ionic/angular";
import { StoragelocationService } from "../services/storagelocation.service";
import { environment } from "src/environments/environment";
import { BobjectsService } from "src/app/services/bobjects.service";
import { UtilityService } from "../services/utility.service";
import { GeolocationsService } from "../services/geolocations.service";
import { TranslationService } from "../services/translation-service.service";

@Component({
  selector: "app-book-store",
  templateUrl: "./book-store.page.html",
  styleUrls: ["./book-store.page.scss"],
  providers: [BookstoreService, StoragelocationService],
})
export class BookStorePage implements OnInit, OnDestroy {
  user: FormGroup;
  tempArray: any = [];
  storageArea: any;
  fromQRCode: any;
  isfromQRCode: boolean = false;
  isSupervisor: boolean = false;

  qrcode_code;
  qrcode_description;
  coords;
  constructor(
    public formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public bObject: BobjectsService,
    private commonService: CommonService,
    private bookstoreService: BookstoreService,
    private storagelocationService: StoragelocationService,
    private alertCtrl: AlertController,
    public utility: UtilityService,
    private router: Router,
    public geo: GeolocationsService,
    public translation: TranslationService
  ) {
    this.isSupervisor = true; // localStorage.getItem(environment.IS_SUPERVISOR) == 'true';

    this.user = this.formBuilder.group({
      area: ["", Validators.compose([Validators.required])],
      name: [
        "",
        Validators.compose([
          Validators.minLength(2),
          Validators.maxLength(1000),
          Validators.pattern("^[A-Z a-z 0-9]*$"),
        ]),
      ],
      latitude: [""],
      longitude: [""],
    });

    this.commonService.shipmentData.subscribe((result) => {
      this.tempArray = result;
    });

    this.commonService.fromQRCode.subscribe((result) => {
      this.fromQRCode = result;
      console.log(
        this.fromQRCode,
        null != this.fromQRCode && this.fromQRCode.length > 0
      );
      if (null != this.fromQRCode && this.fromQRCode.length > 0) {
        this.isfromQRCode = true;
      }
    });

    this.storagelocationService.getStorageAreas().subscribe((data) => {
      this.storageArea = data.responseData.entities;
    });

    this.qrcode_code = localStorage.getItem("qrcode_code")
      ? JSON.parse(localStorage.getItem("qrcode_code"))
      : "";
    localStorage.removeItem("qrcode_code");
    this.qrcode_description = localStorage.getItem("qrcode_description");
    localStorage.removeItem("qrcode_description");
  }

  ngOnInit() {
    setTimeout(async () => {
      if (undefined != localStorage.getItem(environment.SHIPMENT_STORAGE)) {
        this.user.controls["area"].setValue(
          parseInt(localStorage.getItem(environment.SHIPMENT_STORAGE))
        );
        this.user.controls["name"].setValue(
          localStorage.getItem(environment.SHIPMENT_COMMENT)
        );
      }
    }, 400);
  }

  ngOnDestroy(): void {
    //this.commonService.reserveSource.unsubscribe();
  }

  async onSubmit(formData) {
    localStorage.setItem(environment.SHIPMENT_STORAGE, formData.value.area);
    localStorage.setItem(environment.SHIPMENT_COMMENT, formData.value.name);

    // let obj = await this.bObject.prepareObject();
    // this.user.controls['latitude'].setValue(obj['latitude']);
    // this.user.controls['longitude'].setValue(obj['longitude']);
    // formData = this.user;
    this.tempArray = [localStorage.getItem("qrcode_id")];
    let geoPermission = await this.geo.checkLocations();
    if (geoPermission) {
      this.coords = await this.geo.getCurrentLocationCoordinates();
      console.log("getting coordinates,", this.coords);
      if (this.coords) {
        this.user.controls["latitude"].setValue(this.coords.latitude);
        this.user.controls["longitude"].setValue(this.coords.longitude);
        this.bookstoreService
          .insertShipment(formData, this.tempArray)
          .subscribe(
            async (res) => {
              if (null != this.fromQRCode && this.fromQRCode.length > 0) {
                let alert = await this.alertCtrl.create({
                  message: "Booking done",
                  buttons: [
                    {
                      text: "DONE",
                      handler: (data) => {
                        this.router.navigateByUrl("home");
                      },
                    },
                    {
                      text: "SCAN MORE",
                      handler: (data) => {
                        this.router.navigateByUrl("readqrcode");
                      },
                    },
                  ],
                });
                alert.present();
              } else {
                let alert = await this.alertCtrl.create({
                  message: "Booking done",
                  buttons: [
                    {
                      text: "HOME",
                      handler: (data) => {
                        this.router.navigateByUrl("check-boxes/2");
                      },
                    },
                  ],
                });
                alert.present();
              }
            },
            async (error) => {
              let alert = await this.alertCtrl.create({
                message: "Error Occured",
                buttons: [
                  {
                    text: "OK",
                    handler: (data) => {
                      if (this.fromQRCode) {
                        this.router.navigateByUrl("home");
                      } else {
                        this.router.navigateByUrl("check-boxes/2");
                      }
                    },
                  },
                ],
              });
              alert.present();
            }
          );
      } else {
        let msg = await this.translation.getTranslateKey(
          "please_turn_on_location_permission"
        );
        this.utility.showAlert(msg);
      }
    } else {
      let msg = await this.translation.getTranslateKey(
        "please_turn_on_location_permission"
      );
      this.utility.showAlert(msg);
    }
  }

  submitDisabled() {
    if (this.isfromQRCode) {
      return this.user.invalid;
    } else {
      if (!this.isSupervisor) {
        return true;
      } else {
        return this.user.invalid;
      }
    }
  }

  cancelProcess() {
    this.router.navigateByUrl("home");
  }
}
