import { Component, Injector, OnInit } from "@angular/core";
import { ContainerService } from "../services/container.service";
import { FormControl } from "@angular/forms";
import { CommonService } from "../services/common.service";
import { Router } from "@angular/router";
import { UtilityService } from "../services/utility.service";
import { ShipmentService } from "../services/shipment.service";
import { BasePage } from "../pages/base-page/base-page";

@Component({
  selector: "app-shipment-list",
  templateUrl: "./shipment-list.page.html",
  styleUrls: ["./shipment-list.page.scss"],
  providers: [ShipmentService],
})
export class ShipmentListPage extends BasePage implements OnInit {
  data: any;
  tempData: any;
  type: string;
  searchControl: FormControl;
  searching: any = false;
  itemClicked: boolean = false;
  tempArray: any = [];
  offset = 0;
  search = "";
  showSpinner = false;
  flag;
  constructor(
    private service: ShipmentService,
    private commonService: CommonService,
    public utility: UtilityService,
    public router: Router,
    injector: Injector
  ) {
    super(injector);
    this.getShipmentDetails();
    this.itemClicked = false;
  }

  async getShipmentDetails() {
    this.showSpinner = true;
    let obj = await this.service.getShipmentList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = obj.data;
    console.log(this.data);
    console.log("problem");
    this.itemClicked = false;
    this.showSpinner = false;
  }

  async doRefresh($event) {
    this.offset = 0;
    let obj = await this.service.getShipmentList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = this.data;
    $event.target.complete();
  }

  async loadData($event) {
    let obj = await this.service.getShipmentList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = [...this.data, ...obj.data];
    $event.target.complete();
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.getShipmentDetails();
    this.tempArray = [];
  }
  async onSearchTerm($event) {
    console.log($event.target.value);
    this.search = $event.target.value;
    this.offset = 0;
    let obj = await this.service.getShipmentList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = obj.data;
    console.log("running");
    this.itemClicked = false;
  }
  containerItemClick($event, params): void {
    console.log(params);
    if (params.isChecked) {
      this.tempArray.push(params.id);
    } else {
      this.tempArray.splice(this.tempArray.indexOf(params.id), 1);
    }
    if (this.tempArray.length == 1) {
      this.itemClicked = true;
    } else {
      this.itemClicked = false;
    }
  }

  bookNow() {
    let checkedData = this.data.filter((x) => x.isChecked == true);
    this.commonService.addShipmentData(checkedData);
    localStorage.setItem("qrcode_id", checkedData[0].id);
    this.nav.push("book-store");
  }

  openContainerDetail(data) {
    console.log("shipment data", data);
    this.nav.push("shipment-list/container-detail", { id: data.id });
  }
}
