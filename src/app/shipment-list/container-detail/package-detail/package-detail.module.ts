import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { PackageDetailPageRoutingModule } from "./package-detail-routing.module";

import { PackageDetailPage } from "./package-detail.page";
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "src/app/components/shared.module";
import { Ng2SearchPipeModule } from "ng2-search-filter";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PackageDetailPageRoutingModule,
    TranslateModule,
    SharedModule,
    Ng2SearchPipeModule,
  ],
  declarations: [PackageDetailPage],
})
export class PackageDetailPageModule {}
