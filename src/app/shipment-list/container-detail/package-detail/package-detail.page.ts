import { Component, Injector, OnInit } from "@angular/core";
import { BasePage } from "src/app/pages/base-page/base-page";
import { ModalService } from "src/app/services/basic/modal.service";
import { ContainerService } from "src/app/services/container.service";
import { PackageService } from "src/app/services/package.service";

@Component({
  selector: "app-package-detail",
  templateUrl: "./package-detail.page.html",
  styleUrls: ["./package-detail.page.scss"],
})
export class PackageDetailPage extends BasePage implements OnInit {
  id;
  public packageDetails = [];
  loading;
  noPackages: String;
  search: String = "";
  constructor(
    public packageService: PackageService,
    public modals: ModalService,
    injector: Injector,
    private containerService: ContainerService
  ) {
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    this.id = this.getQueryParams().id;
    this.loading = true;
    this.packageDetails =
      await this.containerService.getPackageDetailByContainerId(this.id);
    console.log("here are your shipment details", this.packageDetails);
    if (this.packageDetails.length > 0) {
      this.loading = false;
    } else {
      this.loading = false;
      this.noPackages = await this.translation.getTranslateKey("no_packages");
      return null;
    }
  }

  openPackageItemDetail(data) {
    console.log("shipment data", data);
  }

  onSearchTerm($event) {
    this.search = $event.target.value;
    // this.packageDetails = this.packageDetails.filter((item) =>
    //   item.packageNo.includes(this.search)
    // );
  }
  close() {
    this.nav.pop();
  }
}
