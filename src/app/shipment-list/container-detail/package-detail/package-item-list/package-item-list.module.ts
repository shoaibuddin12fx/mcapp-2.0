import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { PackageItemListPageRoutingModule } from "./package-item-list-routing.module";

import { PackageItemListPage } from "./package-item-list.page";
import { TranslateModule } from "@ngx-translate/core";
import { OrderlinelistComponent } from "src/app/components/orderlinelist/orderlinelist.component";
import { SharedModule } from "src/app/components/shared.module";
import { Ng2SearchPipeModule } from "ng2-search-filter";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PackageItemListPageRoutingModule,
    TranslateModule,
    SharedModule,
    Ng2SearchPipeModule,
  ],
  declarations: [PackageItemListPage],
})
export class PackageItemListPageModule {}
