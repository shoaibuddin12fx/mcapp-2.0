import { Component, Injector, OnInit } from "@angular/core";
import { BasePage } from "src/app/pages/base-page/base-page";
import { ModalService } from "src/app/services/basic/modal.service";
import { PackageService } from "src/app/services/package.service";

@Component({
  selector: "app-package-item-list",
  templateUrl: "./package-item-list.page.html",
  styleUrls: ["./package-item-list.page.scss"],
})
export class PackageItemListPage extends BasePage implements OnInit {
  id;
  public packageDetails = [];
  loading;
  noOrderline: String;
  search: String = "";

  constructor(
    public packageService: PackageService,
    public modals: ModalService,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    this.id = this.getQueryParams().id;
    this.loading = true;
    this.packageDetails =
      await this.packageService.getOrderLineDetailByPackageId(this.id);
    console.log("here are your shipment details", this.packageDetails);
    if (this.packageDetails.length > 0) {
      this.loading = false;
    } else {
      this.loading = false;
      this.noOrderline = await this.translation.getTranslateKey("no_OrderLine");
      return null;
    }
  }

  close() {
    this.nav.pop();
  }

  onSearchTerm($event) {
    this.search = $event.target.value;
    // this.packageDetails = this.packageDetails.filter((item) =>
    //   item.description.includes(this.search)
    // );
  }
}
