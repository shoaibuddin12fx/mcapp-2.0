import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PackageItemListPage } from './package-item-list.page';

describe('PackageItemListPage', () => {
  let component: PackageItemListPage;
  let fixture: ComponentFixture<PackageItemListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageItemListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PackageItemListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
