import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PackageDetailPage } from './package-detail.page';

const routes: Routes = [
  {
    path: '',
    component: PackageDetailPage
  },
  {
    path: 'package-item-list',
    loadChildren: () => import('./package-item-list/package-item-list.module').then( m => m.PackageItemListPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PackageDetailPageRoutingModule {}
