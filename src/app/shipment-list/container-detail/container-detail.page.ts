import { Component, Injector, Input, OnInit } from "@angular/core";
import { BasePage } from "src/app/pages/base-page/base-page";
import { ModalService } from "src/app/services/basic/modal.service";
import { ContainerService } from "src/app/services/container.service";
import { ShipmentService } from "src/app/services/shipment.service";

@Component({
  selector: "app-container-detail",
  templateUrl: "./container-detail.page.html",
  styleUrls: ["./container-detail.page.scss"],
})
export class ContainerDetailPage extends BasePage implements OnInit {
  id;
  public containerDetails = [];
  loading = false;
  noContainers: String;
  search = "";
  itemClicked: boolean = false;

  // get id() {
  //   return this._id;
  // }
  // @Input() set id(value: Number) {
  //   this._id = value;
  // }
  constructor(private shipmentService: ShipmentService, injector: Injector) {
    super(injector);
    this.initialize();
  }

  ngOnInit() {}

  async initialize() {
    this.id = this.getQueryParams().id;
    console.log("id", this.id);
    this.loading = true;
    this.containerDetails =
      await this.shipmentService.getContainerDetailByShipmentId(this.id);
    console.log("here are your shipment details", this.containerDetails);
    if (this.containerDetails.length > 0) {
      this.loading = false;
    } else {
      this.loading = false;
      this.noContainers = await this.translation.getTranslateKey(
        "no_container"
      );
      return null;
    }
  }

  async onSearchTerm($event) {
    console.log($event.target.value);
    this.search = $event.target.value;

    // this.containerDetails = this.containerDetails.filter((item) =>
    //   item.containerNo.includes(this.search)
    // );
  }

  openContainerPackages(data) {
    console.log("shipment data", data);
    this.nav.push("shipment-list/container-detail/package-detail", {
      id: data.id,
    });
  }

  close() {
    this.nav.pop();
  }
}
