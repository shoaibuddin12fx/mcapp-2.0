import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ContainerDetailPageRoutingModule } from "./container-detail-routing.module";

import { ContainerDetailPage } from "./container-detail.page";
import { TranslateModule, TranslatePipe } from "@ngx-translate/core";
import { ContainerlistComponent } from "src/app/components/containerlist/containerlist.component";
import { SharedModule } from "src/app/components/shared.module";
import { Ng2SearchPipeModule } from "ng2-search-filter";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContainerDetailPageRoutingModule,
    TranslateModule,
    SharedModule,
    Ng2SearchPipeModule,
  ],
  declarations: [ContainerDetailPage],
})
export class ContainerDetailPageModule {}
