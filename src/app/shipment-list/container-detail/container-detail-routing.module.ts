import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContainerDetailPage } from './container-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ContainerDetailPage
  },
  {
    path: 'package-detail',
    loadChildren: () => import('./package-detail/package-detail.module').then( m => m.PackageDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContainerDetailPageRoutingModule {}
