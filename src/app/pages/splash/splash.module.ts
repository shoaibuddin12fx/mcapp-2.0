import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { SplashPageRoutingModule } from "./splash-routing.module";

import { SplashPage } from "./splash.page";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SplashPageRoutingModule,
    TranslateModule,
  ],
  declarations: [SplashPage],
})
export class SplashPageModule {}
