import { Component, Injector, OnInit } from "@angular/core";
import { BasePage } from "../base-page/base-page";

@Component({
  selector: "app-logs",
  templateUrl: "./logs.page.html",
  styleUrls: ["./logs.page.scss"],
})
export class LogsPage extends BasePage implements OnInit {
  plist = [];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.sqlite.getLogInDatabase().then((v) => {
      console.log("getting logs", { v });
      this.plist = v as any[];
    });
  }

  jsonParse(data) {
    return JSON.parse(data);
  }

  clearLogs() {
    this.sqlite.clearLogInDatabase();
    this.sqlite.getLogInDatabase().then((v) => {
      console.log(v);
      this.plist = v as any[];
    });
  }

  back() {
    this.nav.pop();
  }
}
