import { Component, Injector, Input, OnInit } from "@angular/core";
import { StockService } from "src/app/services/stock.service";
import { threadId } from "worker_threads";
import { BasePage } from "../../base-page/base-page";

@Component({
  selector: "app-stockdetail",
  templateUrl: "./stockdetail.component.html",
  styleUrls: ["./stockdetail.component.scss"],
})
export class StockdetailComponent extends BasePage implements OnInit {
  @Input() entry;
  list;
  recievedDate;
  code;
  stockDetails;
  showAndHideText = false;
  loading = true;
  constructor(injector: Injector, public stockService: StockService) {
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    console.log(this.entry);
    this.stockDetails = await this.stockService.getStockItemDetails(this.entry);

    if (this.stockDetails) {
      this.loading = false;
      this.recievedDate = new Date(this.stockDetails["receivedDate"]);

      this.recievedDate =
        this.recievedDate.getDate() +
        "." +
        this.recievedDate.getMonth() +
        "." +
        this.recievedDate.getFullYear();

      console.log(this.recievedDate);
    }
    this.stockDetails["description"] = this.stockDetails["decription"];

    // console.log(this.stockDetails);
    // if (this.entry.siteEta) {
    //   this.list = Array.from(this.entry.siteEta);
    //   this.siteEtaList = this.list.join();
    //   this.siteEtaList = this.siteEtaList.replace("[", "");
    //   this.siteEtaList = this.siteEtaList.replace("]", "");
    //   console.log(this.siteEtaList);
    // } else {
    //   this.list = this.entry[0].locations;
    //   this.code = this.list[0].code;
    //   console.log("getting location list", this.list);
    // }
  }

  close() {
    this.modals.dismiss();
  }

  hideAndUnhideText() {
    this.showAndHideText = !this.showAndHideText;
  }
}
