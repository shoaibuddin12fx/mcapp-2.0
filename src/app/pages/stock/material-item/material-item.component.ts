import { Component, Injector, Input, OnInit } from "@angular/core";
import { StockService } from "src/app/services/stock.service";
import { BasePage } from "../../base-page/base-page";

@Component({
  selector: "app-material-item",
  templateUrl: "./material-item.component.html",
  styleUrls: ["./material-item.component.scss"],
})
export class MaterialItemComponent extends BasePage implements OnInit {
  @Input() entry;
  list;
  siteEta;
  code;
  materialDetails;
  showAndHideText = false;
  loading = true;
  constructor(injector: Injector, public stockService: StockService) {
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    console.log(this.entry);
    this.materialDetails = await this.stockService.getOrderItemDetails(
      this.entry
    );

    if (this.materialDetails) {
      this.loading = false;

      if (this.materialDetails["siteETA"] != null) {
        this.siteEta = new Date(this.materialDetails["siteETA"]);
        console.log("getting full date: ", this.siteEta.getDay());
        this.siteEta =
          this.siteEta.getDate() +
          "." +
          this.siteEta.getMonth() +
          "." +
          this.siteEta.getFullYear();
      }
    }

    // console.log(this.materialDetails);
    // if (this.entry.siteEta) {
    //   this.list = Array.from(this.entry.siteEta);
    //   this.siteEtaList = this.list.join();
    //   this.siteEtaList = this.siteEtaList.replace("[", "");
    //   this.siteEtaList = this.siteEtaList.replace("]", "");
    //   console.log(this.siteEtaList);
    // } else {
    //   this.list = this.entry[0].locations;
    //   this.code = this.list[0].code;
    //   console.log("getting location list", this.list);
    // }
  }

  close() {
    this.modals.dismiss();
  }

  hideAndUnhideText() {
    this.showAndHideText = !this.showAndHideText;
  }
}
