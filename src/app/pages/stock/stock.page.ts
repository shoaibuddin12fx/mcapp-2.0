import { Component, Injector, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { BobjectsService } from "src/app/services/bobjects.service";
import { LoadingService } from "src/app/services/loading-service";
import { StockService } from "src/app/services/stock.service";
import { TranslationService } from "src/app/services/translation-service.service";
import { UtilityService } from "src/app/services/utility.service";
import { SuppliersService } from "src/app/suppliers.service";
import { environment } from "src/environments/environment";
import { BasePage } from "../base-page/base-page";
import { MaterialItemComponent } from "./material-item/material-item.component";
import { StockdetailComponent } from "./stockdetail/stockdetail.component";

@Component({
  selector: "app-stock",
  templateUrl: "./stock.page.html",
  styleUrls: ["./stock.page.scss"],
})
export class StockPage extends BasePage implements OnInit {
  aForm: FormGroup;
  submitted = false;
  showSearch = true;
  data = [];
  filterTerm;
  offset = 0;
  search = "";
  showSpinner = false;
  mode = "Stock Items";
  page;
  suppliers;
  stockItemFilter;
  showMode = true;
  recordsFound = true;
  constructor(
    injector: Injector,
    public router: Router,
    public stockService: StockService,
    public utility: UtilityService,
    public translation: TranslationService,
    public bObject: BobjectsService,
    public supplies: SuppliersService,
    public loadingService: LoadingService
  ) {
    super(injector);
    if (!this.showSearch) {
      this.getStockDetails();
    }
    this.initialize();
  }

  resetSearch() {
    console.log("ping");
    this.showSearch = true;
    this.showMode = true;
    this.aForm.reset();
  }

  ionViewWillEnter() {
    // if(this.router.url == 'stock'){
    //   this.showSearch = true
    //   console.log(this.showSearch);
    // }
  }

  ngOnInit() {
    this.events.subscribe("stock:reset", this.resetSearch.bind(this));
    this.setupForm();
  }

  async initialize() {
    this.suppliers = await this.initializeSuppliers();
  }

  setupForm() {
    this.aForm = this.formBuilder.group({
      workNo: [""],
      posNo: [""],
      description: [""],
      partNo: [""],
      suppliers: [],
      poNo: [""],
      drawingNo: [""],
    });
  }

  async getStockDetails() {
    this.showSpinner = true;
    let obj = await this.stockService.getStockList(
      this.search,
      this.offset,
      this.mode
    );
    console.log("getting stocks", { obj });
    this.offset = obj.offset;
    this.data = obj.data;
    console.log("problem");
    this.showSpinner = false;
  }

  async searchForm() {
    //     {"username":"ProjectOffice","token":"abcd",
    // "  ":1,
    // "texts":[],
    // "posNo":"",
    // "drawingNo":"703078764",
    // "description":"",
    // "productCode":"",
    // "poNo":""
    // }
    this.data = [];
    let formdata = this.aForm.value;

    formdata["projectId"] = localStorage.getItem(environment.PROJECT_CODE);

    if (!formdata["projectId"]) {
      this.translation.getTranslateKey("project_selection_error").then((v) => {
        this.utility.presentFailureToast(v);
      });

      this.router.navigateByUrl("setting");

      return;
    }

    if (
      (formdata["workNo"] == "" || formdata["workNo"] == null) &&
      (formdata["posNo"] == "" || formdata["posNo"] == null) &&
      (formdata["description"] == "" || formdata["description"] == null) &&
      (formdata["partNo"] == "" || formdata["partNo"] == null) &&
      (formdata["suppliers"] == "" || formdata["suppliers"] == null) &&
      (formdata["poNo"] == "" || formdata["poNo"] == null) &&
      (formdata["drawingNo"] == "" || formdata["drawingNo"] == null)
    ) {
      this.utility.presentFailureToast(
        "At least one field is required to search"
      );
      return;
    }

    // formdata["texts"] = formdata["areaNo"] != "" ? [formdata["areaNo"]] : [];
    delete formdata["areaNo"];

    this.submitted = true;
    this.showSpinner = true;
    // this.utility.showLoader();
    this.showSearch = false;
    this.showMode = false;
    this.recordsFound = true;
    console.log(formdata);

    setTimeout(async () => {
      var obj: any = await this.stockService.getStockListByUrl(
        formdata,
        this.mode
      );
      // this.utility.hideLoader();
      this.offset = obj.offset;
      this.data = obj.data;
      if (this.data.length == 0) {
        this.recordsFound = false;
        console.log("no items", this.recordsFound);
      } else if (this.data.length != 0) {
        this.recordsFound = true;
      }
      console.log("items", this.recordsFound);
      this.showSpinner = false;
    }, 1000);
  }

  async loadData($event) {
    if (!this.showSearch) {
      let obj = await this.stockService.getStockList(
        this.search,
        this.offset,
        this.mode
      );
      this.offset = obj.offset;
      this.data = [...this.data, ...obj.data];
      $event.target.complete();
    }
  }

  delayTimer;
  async onSearchTerm($event) {
    console.log("Working");
    let v = $event.target.value;
    clearTimeout(this.delayTimer);

    this.delayTimer = setTimeout(async () => {
      this.offset = 0;
      let obj = await this.stockService.getStockList(v, this.offset, this.mode);
      console.log(obj);
      this.offset = obj.offset;
      this.data = obj.data;
    }, 500);

    // let searchVal = searchTerm.detail.value;
    // this.data = this.tempData;

    // if (searchVal && searchVal.trim() !== '') {

    //   this.data = this.data.filter(item => {
    //     return item.containerNo.indexOf(searchVal) > -1;
    //   });
    // }
  }

  async initializeSuppliers(): Promise<any> {
    return new Promise(async (resolve) => {
      const loadingMessage = await this.translation.getTranslateKey("loading");
      const show = await this.utility.showLoader(loadingMessage);
      this.supplies.getSuppliers().subscribe(async (data) => {
        console.log(data);
        const loading = await this.utility.hideLoader();
        resolve(data.responseData.entities);
      });
    });
  }

  async showStockDetails(entry) {
    console.log(this.mode);
    if (this.mode == "Stock Items") {
      // let stockData = await this.getStockItemDetails(entry);
      console.log("hello");
      const modal = await this.modals.present(StockdetailComponent, {
        entry: entry,
      });
    } else if ("Master Search") {
      // let masterSearchData = await this.getOrderItemDetails(entry);
      // console.log(masterSearchData);
      const modal = await this.modals.present(MaterialItemComponent, {
        entry: entry,
      });
    }
  }

  hideandShowSearch() {
    this.showSearch = !this.showSearch;
    this.showMode = !this.showMode;
  }
}
