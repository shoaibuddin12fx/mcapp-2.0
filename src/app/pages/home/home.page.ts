import { AppSettings } from "./../../services/app-settings";
import { Component, OnInit } from "@angular/core";
import { HomeService } from "./../../services/home-service";
import { ModalController } from "@ionic/angular";
import { IntroPage } from "../intro-page/intro-page.page";
import { EventsService } from "src/app/services/events.service";
import { SettingPage } from "src/app/setting/setting.page";
import { PackageService } from "src/app/services/package.service";
import { ContainerService } from "src/app/services/container.service";
import { environment } from "src/environments/environment";
import { ProjectService } from "src/app/services/project.service";
import { StatuscodeService } from "src/app/services/statuscode.service";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
  providers: [HomeService],
})
export class HomePage implements OnInit {
  item;
  offset = 0;
  search = "";
  dataLoaded = false;
  projectTags;
  statusCodes;
  constructor(
    private homeService: HomeService,
    private packageService: PackageService,
    private containerService: ContainerService,
    public projectService: ProjectService,
    public statuscodeService: StatuscodeService,
    public modalController: ModalController,
    public events: EventsService
  ) {
    this.item = this.homeService.getData();
    let showWizard = localStorage.getItem("SHOW_START_WIZARD");

    if (AppSettings.SHOW_START_WIZARD && !showWizard) {
      this.openModal();
    }

    console.log("token refresh");
    homeService.tokenRefresh();
    console.log("home  page loaded");
    this.events.subscribe("Settings: settings updated", () => {
      console.log("Recieving event");
    });
    this.initialize();
  }

  async initialize() {
    var projectRes = await this.getProjects();
    var statusCodeRes = await this.getStatusCodes();
    localStorage.setItem(environment.PROJECT_CODE, this.projectTags[0].id);
    let statusCodeEntity = this.statusCodes.map((val) => {
      return val.id;
    });
    localStorage.setItem(environment.STATUS_CODE, statusCodeEntity);

    console.log("Project Tags", this.projectTags[0].id);
    console.log("Status COdes", statusCodeEntity);
    // localStorage.setItem(environment.PROJECT_CODE,);
    // localStorage.setItem(environment.STATUS_CODE,);
  }
  ngOnInit() {
    this.events.subscribe(
      "settings:modal-open",
      this.openSettingsModal.bind(this)
    );
  }

  async getProjects() {
    return new Promise((resolve) => {
      this.projectService.getProjectTags().subscribe((data) => {
        this.projectTags = data.responseData.entities;
        resolve(true);
      });
    });
  }

  async getStatusCodes() {
    return new Promise((resolve) => {
      this.statuscodeService.getStatusCode().subscribe((data) => {
        this.statusCodes = data.responseData.entities;
        resolve(true);
      });
    });
  }
  // async getPackageData() {
  //   let packagelist = await this.packageService.getPackageList();

  //   if (packagelist) {
  //     this.dataLoaded = true;
  //     console.log("data loaded: ", this.dataLoaded);
  //   }
  // }

  // async getContainerData() {
  //   let containerlist = await this.containerService.getContainerList();

  //   if (containerlist) {
  //     this.dataLoaded = true;
  //     console.log("data loaded: ", this.dataLoaded);
  //   }
  // }

  async openModal() {
    let modal = await this.modalController.create({ component: IntroPage });
    return await modal.present();
  }

  async openSettingsModal() {
    let modal = await this.modalController.create({ component: SettingPage });
    return await modal.present();
  }
}
