import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { RouterModule } from "@angular/router";
import { SharedModule } from "./../../components/shared.module";

import { ItemDetailsLoginPage } from "./item-details-login.page";
import { TranslateModule } from "@ngx-translate/core";
import { HeaderMenuComponent } from "src/app/components/header-menu/header-menu.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule,
    RouterModule.forChild([
      {
        path: "",
        component: ItemDetailsLoginPage,
      },
    ]),
  ],
  declarations: [ItemDetailsLoginPage, HeaderMenuComponent],
  exports: [ItemDetailsLoginPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ItemDetailsLoginPageModule {}
