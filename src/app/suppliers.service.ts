import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { LoadingService } from "./services/loading-service";

@Injectable({
  providedIn: "root",
})
export class SuppliersService {
  constructor(
    private loadingService: LoadingService,
    private http: HttpClient
  ) {}

  public getSuppliers(): Observable<any> {
    var url = localStorage.getItem("app_Url") + "/rest/projects/tags";
    var headers = new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    });

    let request = {
      username: localStorage.getItem(environment.USER_NAME),
      token: localStorage.getItem(environment.TOKEN),
      accountId: localStorage.getItem(environment.ACCOUNT_ID),
      locale: "en_US",
      isMobile: true,
    };
    return this.http.post(url, request, { headers: headers });
  }
}
