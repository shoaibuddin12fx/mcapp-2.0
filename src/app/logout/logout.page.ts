import { Component, Injector, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "../../environments/environment";
import { BasePage } from "../pages/base-page/base-page";

@Component({
  selector: "app-logout",
  templateUrl: "./logout.page.html",
  styleUrls: ["./logout.page.scss"],
})
export class LogoutPage extends BasePage implements OnInit {
  constructor(public router: Router, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    // this.events.publish("logout");
  }
}
