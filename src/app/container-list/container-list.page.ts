import { Component, Injector, OnInit } from "@angular/core";
import { ContainerService } from "../services/container.service";
import { FormControl } from "@angular/forms";
import { CommonService } from "../services/common.service";
import { Router } from "@angular/router";
import { UtilityService } from "../services/utility.service";
import { BasePage } from "../pages/base-page/base-page";

@Component({
  selector: "app-container-list",
  templateUrl: "./container-list.page.html",
  styleUrls: ["./container-list.page.scss"],
  providers: [ContainerService],
})
export class ContainerListPage extends BasePage implements OnInit {
  data: any;
  tempData: any;
  type: string;
  searchControl: FormControl;
  searching: any = false;
  itemClicked: boolean = false;
  tempArray: any = [];
  offset = 0;
  search = "";
  showSpinner = false;
  showBookingButton = false;
  constructor(
    private service: ContainerService,
    private commonService: CommonService,
    public utility: UtilityService,
    public router: Router,
    injector: Injector
  ) {
    super(injector);
    this.getContainerDetails();
    this.itemClicked = false;
  }

  async getContainerDetails() {
    this.showSpinner = true;
    let obj = await this.service.getContainerList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = obj.data;
    console.log(obj);
    console.log("problem");
    this.itemClicked = false;

    this.showSpinner = false;
  }

  async doRefresh($event) {
    this.offset = 0;
    let obj = await this.service.getContainerList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = this.data;
    $event.target.complete();
  }

  async loadData($event) {
    let obj = await this.service.getContainerList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = [...this.data, ...obj.data];
    $event.target.complete();
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.getContainerDetails();
    this.tempArray = [];
  }
  async onSearchTerm($event) {
    console.log($event.target.value);
    this.search = $event.target.value;
    this.offset = 0;
    let obj = await this.service.getContainerList(this.search, this.offset);
    this.offset = obj.offset;
    this.data = obj.data;
    // let searchVal = searchTerm.detail.value;
    // this.data = this.tempData;

    // if (searchVal && searchVal.trim() !== '') {

    //   this.data = this.data.filter(item => {
    //     return item.containerNo.indexOf(searchVal) > -1;
    //   });
    // }
  }
  containerItemClick($event, params): void {
    console.log(params);
    if (params.isChecked) {
      this.tempArray.push(params.id);
    } else {
      this.tempArray.splice(this.tempArray.indexOf(params.id), 1);
    }
    if (this.tempArray.length == 1) {
      this.itemClicked = true;
    } else {
      this.itemClicked = false;
    }
  }

  bookNow() {
    let checkedData = this.data.filter((x) => x.isChecked == true);
    this.commonService.addContainerData(checkedData);
    localStorage.setItem("qrcode_id", checkedData[0].id);
    this.router.navigateByUrl("book-container");
  }

  openPackageDetail(data) {
    this.nav.push("container-list/packageslist", {
      id: data.id,
    });
  }
}
