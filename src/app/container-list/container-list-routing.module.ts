import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContainerListPage } from './container-list.page';

const routes: Routes = [
  {
    path: '',
    component: ContainerListPage
  },  {
    path: 'packageslist',
    loadChildren: () => import('./packageslist/packageslist.module').then( m => m.PackageslistPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContainerListPageRoutingModule {}
