import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PackageItemListPage } from './package-item-list.page';

const routes: Routes = [
  {
    path: '',
    component: PackageItemListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PackageItemListPageRoutingModule {}
