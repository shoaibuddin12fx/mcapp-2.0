import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { PackageslistPageRoutingModule } from "./packageslist-routing.module";

import { PackageslistPage } from "./packageslist.page";
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "src/app/components/shared.module";
import { Ng2SearchPipeModule } from "ng2-search-filter";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PackageslistPageRoutingModule,
    TranslateModule,
    SharedModule,
    Ng2SearchPipeModule,
  ],
  declarations: [PackageslistPage],
})
export class PackageslistPageModule {}
