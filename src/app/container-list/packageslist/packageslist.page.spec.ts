import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PackageslistPage } from './packageslist.page';

describe('PackageslistPage', () => {
  let component: PackageslistPage;
  let fixture: ComponentFixture<PackageslistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageslistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PackageslistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
