import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PackageslistPage } from './packageslist.page';

const routes: Routes = [
  {
    path: '',
    component: PackageslistPage
  },
  {
    path: 'package-item-list',
    loadChildren: () => import('./package-item-list/package-item-list.module').then( m => m.PackageItemListPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PackageslistPageRoutingModule {}
