import { Component, Injector, OnInit } from "@angular/core";
import { BasePage } from "src/app/pages/base-page/base-page";
import { ModalService } from "src/app/services/basic/modal.service";
import { ContainerService } from "src/app/services/container.service";

@Component({
  selector: "app-packageslist",
  templateUrl: "./packageslist.page.html",
  styleUrls: ["./packageslist.page.scss"],
})
export class PackageslistPage extends BasePage implements OnInit {
  id;
  public packageDetails = [];
  loading;
  noPackages: String;
  search = "";
  // get id() {
  //   return this._id;
  // }
  // @Input() set id(value: Number) {
  //   this._id = value;
  // }

  constructor(
    public containerService: ContainerService,
    public modals: ModalService,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    this.id = this.getQueryParams().id;
    this.loading = true;
    this.packageDetails =
      await this.containerService.getPackageDetailByContainerId(this.id);
    console.log("here are your shipment details", this.packageDetails);
    if (this.packageDetails.length > 0) {
      this.loading = false;
    } else {
      this.loading = false;
      this.noPackages = await this.translation.getTranslateKey("no_packages");
      return null;
    }
  }

  async onSearchTerm($event) {
    console.log($event.target.value);
    this.search = $event.target.value;

    // this.packageDetails = this.packageDetails.filter((item) => {
    //   return item.packageNo.includes(this.search);
    // });
  }

  openPackageDetail(data) {
    this.nav.push("container-list/packageslist/package-item-list", {
      id: data.id,
    });
  }

  close() {
    this.nav.pop();
  }
}
