import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../services/common.service';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-package-details',
  templateUrl: './package-details.page.html',
  styleUrls: ['./package-details.page.scss'],
})
export class PackageDetailsPage implements OnInit {

  @Input() description: any;
  @Input() poNo: any;
  @Input() qty: any;
  @Input() containerNo: any;
  @Input() gross: any;
  @Input() height: any;
  @Input() length: any;
  @Input() volume: any;
  @Input() width: any;
  @Input() net: any;
  @Input() supplierText: any;
  @Input() packageNo: any;
  

  constructor(private commonService:CommonService,
    public modalCtrl: ModalController) { }

  ngOnInit() {
  }

  closeModel(){
    this.modalCtrl.dismiss();
  }

}

