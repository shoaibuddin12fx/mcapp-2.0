import { Component } from "@angular/core";
import { ExportService } from "./services/export-service";
import { Platform } from "@ionic/angular";
import { SplashScreen } from "@capacitor/splash-screen";
// import { StatusBar } from '@ionic-native/status-bar/ngx';
import { StatusBar } from "@capacitor/status-bar";
import { MenuService } from "./services/menu-service";
import { NavController } from "@ionic/angular";
import { environment } from "../environments/environment";
import { Router } from "@angular/router";
import { LoginService } from "./services/login-service";
import { CommonService } from "./services/common.service";
import { TranslationService } from "./services/translation-service.service";
import { EventsService } from "./services/events.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { SqliteService } from "./services/sqlite.service";
import { UtilityService } from "./services/utility.service";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { NavService } from "./services/nav.service";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
  providers: [MenuService, ExportService, CommonService],
})
export class AppComponent {
  public appPages = [];
  headerMenuItem = {
    image: "",
    title: "",
    background: "",
  };
  isEnabledRTL: boolean = false;

  constructor(
    private platform: Platform,
    private menuService: MenuService,
    private navController: NavController,
    private router: Router,
    public translationService: TranslationService,
    public events: EventsService,
    public sqlite: SqliteService,
    public androidPermissions: AndroidPermissions,
    public utilityService: UtilityService,
    public nav: NavService
  ) {
    this.initializeApp();
    this.isEnabledRTL = localStorage.getItem("isEnabledRTL") == "true";
    //console.log(JSON.stringify(exportService.export()));
    this.events.subscribe("Logged In", () => {
      console.log("Event subscribed!");
      this.appPages = this.menuService.getAllThemes();
    });

    if (localStorage.getItem("hasLoggedIn") === "true") {
      this.appPages = this.menuService.getAllThemes();
    }
    // this.appPages = this.menuService.getAllThemes();
    this.events.subscribe("logout", () => {
      this.appPages = [];
    });
    this.headerMenuItem = this.menuService.getDataForTheme(null);
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      this.askStoragePermission();
      const flag = await this.sqlite.initialize();
      if (!flag) {
        this.exitApp();
      }
      this.setRTL();
      StatusBar.setOverlaysWebView({ overlay: false });
      StatusBar.setBackgroundColor({ color: "#000000" });
      this.nav.push("splash");
      SplashScreen.hide();
      // this.splashScreen.hide();
      const url = localStorage.getItem("app_Url");
      if (!url) {
        localStorage.setItem("app_Url", environment.SERVER_URL_ONE);
      }
      // this.events.subscribe("user:logout", this.logout.bind(this))
      // const flag = await this.sqlite.initializeDatabase();
      // if(!flag){
      //   this.exitApp();
      // }
      this.platform.backButton.subscribeWithPriority(1, () => {
        console.log("hit back btn");
      });
    });
  }
  setRTL() {
    document
      .getElementsByTagName("html")[0]
      .setAttribute("dir", this.isEnabledRTL ? "rtl" : "ltr");
    this.translationService.setDefaultLanguage();
  }

  logout() {
    this.openPage("logout");
  }

  exitApp() {
    navigator["app"].exitApp();
  }

  openPage(page) {
    console.log(page.url, page.url == "stock");
    if (page.url == "stock") {
      this.events.publish("stock:reset");
    }

    if (page.url == "settings") {
      // this.navController.navigateRoot([page.url], {});
      // send an event to dashboard and open modal for settings
      this.events.publish("settings:modal-open");
    }

    if (page.url == "logout") {
      let envProject = localStorage.getItem(environment.PROJECT_CODE);
      let envStatuscodes = localStorage.getItem(environment.STATUS_CODE);
      localStorage.setItem(environment.PROJECT_CODE, envProject);
      localStorage.setItem(environment.STATUS_CODE, envStatuscodes);
      this.router.navigateByUrl("login/0");
    } else {
      this.navController.navigateRoot([page.url], {});
    }

    console.log(page.url);
  }

  async askStoragePermission() {
    this.androidPermissions
      .checkPermission(
        this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
      )
      .then(async (result) => {
        if (result.hasPermission) {
          return true;
        } else {
          this.androidPermissions
            .requestPermission(
              this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
            )
            .then((result) => {
              if (result.hasPermission) {
                return true;
              } else {
                this.exitApp();
              }
            });
        }
      });
  }
}
