import { Component, Injector, OnInit } from "@angular/core";
import { BasePage } from "src/app/pages/base-page/base-page";
import { ModalService } from "src/app/services/basic/modal.service";
import { ContainerService } from "src/app/services/container.service";
import { PackageService } from "src/app/services/package.service";

@Component({
  selector: "app-packages",
  templateUrl: "./packages.page.html",
  styleUrls: ["./packages.page.scss"],
})
export class PackagesPage extends BasePage implements OnInit {
  id;
  public orderLineDetails = [];
  loading;
  noOrderline: String;
  search: String = "";
  constructor(
    public packageService: PackageService,
    public modals: ModalService,
    injector: Injector,
    private containerService: ContainerService
  ) {
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    this.id = this.getQueryParams().id;
    this.loading = true;
    this.orderLineDetails =
      await this.packageService.getOrderLineDetailByPackageId(this.id);
    console.log("here are your shipment details", this.orderLineDetails);
    if (this.orderLineDetails.length > 0) {
      this.loading = false;
    } else {
      this.loading = false;
      this.noOrderline = await this.translation.getTranslateKey("no_OrderLine");
      return null;
    }
  }

  onSearchTerm($event) {
    this.search = $event.target.value;
    // this.orderLineDetails = this.orderLineDetails.filter((item) =>
    //   item.description.includes(this.search)
    // );
  }
  openPackageItemDetail(data) {
    this.nav.push("package-list/packages/orderline", { id: data.id });
    // const res = await this.modals.present(OrderlinesDetailComponent, {
    //   id: data.id,
    // });
  }

  close() {
    this.nav.pop();
  }
}
