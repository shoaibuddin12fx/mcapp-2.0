import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { PackagesPageRoutingModule } from "./packages-routing.module";

import { PackagesPage } from "./packages.page";
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "src/app/components/shared.module";
import { Ng2SearchPipeModule } from "ng2-search-filter";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PackagesPageRoutingModule,
    TranslateModule,
    SharedModule,
    Ng2SearchPipeModule,
  ],
  declarations: [PackagesPage],
})
export class PackagesPageModule {}
