import { Component, OnInit, Input } from "@angular/core";
import { NavService } from "src/app/services/nav.service";

@Component({
  selector: "app-containerlist",
  templateUrl: "./containerlist.component.html",
  styleUrls: ["./containerlist.component.scss"],
})
export class ContainerlistComponent implements OnInit {
  _item;
  get item() {
    return this._item;
  }
  @Input() set item(value: any) {
    this._item = value;
  }
  constructor(public nav: NavService) {}

  ngOnInit() {}

  openContainerPackages(item) {
    // console.log("shipment data", data);
    this.nav.push("shipment-list/container-detail/package-detail", {
      id: item.id,
    });
  }
}
