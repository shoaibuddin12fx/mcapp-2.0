import { Component, Input, OnInit } from "@angular/core";
import { NavService } from "src/app/services/nav.service";

@Component({
  selector: "app-packagelist",
  templateUrl: "./packagelist.component.html",
  styleUrls: ["./packagelist.component.scss"],
})
export class PackagelistComponent implements OnInit {
  _item;
  get item() {
    return this._item;
  }
  @Input() set item(value: any) {
    this._item = value;
  }
  constructor(public nav: NavService) {}

  ngOnInit() {
    console.log(this.item);
  }

  openPackageItemDetail(item) {
    this.nav.push(
      "shipment-list/container-detail/package-detail/package-item-list",
      {
        id: item.id,
      }
    );
  }
}
