import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "app-orderlinelist",
  templateUrl: "./orderlinelist.component.html",
  styleUrls: ["./orderlinelist.component.scss"],
})
export class OrderlinelistComponent implements OnInit {
  _item;
  get item() {
    return this._item;
  }
  @Input() set item(value: any) {
    this._item = value;
  }
  constructor() {}

  ngOnInit() {
    console.log("got orderline item", this.item);
  }
}
