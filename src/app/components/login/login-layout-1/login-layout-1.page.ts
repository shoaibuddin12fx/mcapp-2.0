import {
  Component,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChange,
  SimpleChanges,
  Injector,
} from "@angular/core";
import { BasePage } from "src/app/pages/base-page/base-page";

@Component({
  selector: "cs-login-layout-1",
  templateUrl: "login-layout-1.page.html",
  styleUrls: ["login-layout-1.page.scss"],
})
export class LoginLayout1Page extends BasePage implements OnChanges {
  @Input() data: any;

  _loading: boolean;
  get loading(): boolean {
    return this._loading;
  }
  @Input() set loading(value: boolean) {
    this._loading = value;
  }

  @Output() onLogin = new EventEmitter();
  @Output() onRegister = new EventEmitter();
  @Output() onSkip = new EventEmitter();
  @Output() onFacebook = new EventEmitter();
  @Output() onTwitter = new EventEmitter();
  @Output() onGoogle = new EventEmitter();
  @Output() onPinterest = new EventEmitter();

  public isUsernameValid = true;
  public isPasswordValid = true;

  item = {
    username: "",
    password: "",
  };

  constructor(injector: Injector) {
    super(injector);

    let params = localStorage.getItem("login_email_password");
    if (params) {
      let json = JSON.parse(params);
      console.log(json);
      this.item.username = json.username;
      this.item.password = json.password;
    }
  }

  ngOnChanges(changes: { [propKey: string]: any }) {
    if (changes["data"] != undefined) {
      this.data = changes["data"].currentValue;
    }
  }

  onLoginFunc(): void {
    if (event) {
      event.stopPropagation();
    }
    if (this.validate()) {
      this.onLogin.emit(this.item);
      this.events.publish("Logged In");
    }
  }

  onRegisterFunc(): void {
    if (event) {
      event.stopPropagation();
    }
    if (this.validate()) {
      this.onRegister.emit(this.item);
    }
  }

  onFacebookFunc(): void {
    this.onFacebook.emit(this.item);
  }

  onTwitterFunc(): void {
    if (event) {
      event.stopPropagation();
    }
    this.onTwitter.emit(this.item);
  }

  onGoogleFunc(): void {
    if (event) {
      event.stopPropagation();
    }
    this.onGoogle.emit(this.item);
  }

  onPinterestFunc(): void {
    if (event) {
      event.stopPropagation();
    }
    this.onPinterest.emit(this.item);
  }

  onSkipFunc(): void {
    if (event) {
      event.stopPropagation();
    }

    this.onSkip.emit(this.item);
  }

  validate(): boolean {
    this.isUsernameValid = true;
    this.isPasswordValid = true;
    if (!this.item.username || this.item.username.length === 0) {
      this.isUsernameValid = false;
    }

    if (!this.item.password || this.item.password.length === 0) {
      this.isPasswordValid = false;
    }

    return this.isPasswordValid && this.isUsernameValid;
  }
}
